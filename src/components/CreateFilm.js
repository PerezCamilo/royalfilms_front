import React, { useState, useEffect } from 'react';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import API from '../providers/api';
import moment from 'moment';
import swalAlert from '../helpers/alert';

export const CreateFilm = () => {
    const [name, setname] = useState('')
    const [languages, setlanguages] = useState('')
    const [clasification, setclasification] = useState('')
    const [hours, sethours] = useState('')
    const [minutes, setminutes] = useState('')
    const [releaseDate, setreleaseDate] = useState('')
    const [urlTrailer, seturlTrailer] = useState('')
    const [synopsis, setsynopsis] = useState('')
    const [distributionFilm, setdistributionFilm] = useState('')
    const [directorName, setdirectorName] = useState('')

    // ARRAYS DROPDOWN
    const [arrLanguages, setarrLanguages] = useState([])
    const [arrClasification, setarrClasification] = useState([])

    const listLanguages = () => {
        API.listLanguajes()
            .then(data => data.json())
            .then(dataJson => {
                if (dataJson.success) {
                    dataJson.data.map((res, i) => {
                        arrLanguages.push({
                            name: res.name,
                            code: res.id
                        })
                        return setarrLanguages([...arrLanguages])
                    })
                }
            })
    }

    const onChangeHours = (e) => {
        const re = /^[0-9\b]+$/;
        let result = re.test(e.target.value)
        if (result)
            sethours(e.target.value)
        else
            sethours('')
    }

    const onChangeMinutes = (e) => {
        const re = /^[0-9\b]+$/;
        let result = re.test(e.target.value)
        if (result)
            setminutes(e.target.value)
        else
            setminutes('')
    }

    const listClasification = () => {
        API.listClasification()
            .then(data => data.json())
            .then(dataJson => {
                if (dataJson.success) {
                    dataJson.data.map((res, i) => {
                        arrClasification.push({
                            name: res.name,
                            code: res.id
                        })
                        return setarrClasification([...arrClasification])
                    })
                } else {
                    console.log(dataJson.message);
                }
            })
            .catch(error => {
                console.log('error ==>', error);
            })
    }

    const createFilm = () => {
        if (name === '') {
            swalAlert('warning', 'Empty name')
        } else if (languages === '') {
            swalAlert('warning', 'Empty language')
        } else if (clasification === '') {
            swalAlert('warning', 'Empty clasification')
        } else if (hours === '') {
            swalAlert('warning', 'Empty hours')
        } else if (minutes === '') {
            swalAlert('warning', 'Empty minutes')
        } else if (releaseDate === '') {
            swalAlert('warning', 'Empty release date')
        } else if (urlTrailer === '') {
            swalAlert('warning', 'Empty url railer')
        } else if (synopsis === '') {
            swalAlert('warning', 'Empty synopsis')
        } else if (distributionFilm === '') {
            swalAlert('warning', 'Empty distribution film')
        } else if (directorName === '') {
            swalAlert('warning', 'Empty director name')
        } else {
            let formatRealeaseDate = moment(releaseDate).format('YYYY-MM-DD')
            let duration = `${hours} : ${minutes}`
            API.createFilms(name, languages.code, clasification.code, duration, formatRealeaseDate, urlTrailer, synopsis, distributionFilm, directorName)
                .then(data => data.json())
                .then(dataJson => {
                    if (dataJson.success) {
                        swalAlert('success', dataJson.message)
                        setname('')
                        setlanguages('')
                        setclasification('')
                        sethours('')
                        setminutes('')
                        setreleaseDate('')
                        seturlTrailer('')
                        setsynopsis('')
                        setdistributionFilm('')
                        setdirectorName('')
                    } else {
                        swalAlert('warning', dataJson.message)
                    }
                })
                .catch(error => {
                    console.log('error ==>', error);
                })
        }
    }

    useEffect(() => {
        listLanguages()
        listClasification()
    }, []);

    return (
        <div className="card">
            <h5>Create film</h5>
            <div className="grid p-fluid mt-3">
                <div className="field col-12 md:col-4">
                    <span className="p-float-label">
                        <InputText type="text" id="name" value={name} onChange={(e) => setname(e.target.value)} />
                        <label htmlFor="name">Name</label>
                    </span>
                </div>

                <div className="field col-12 md:col-4">
                    <span className="p-float-label">
                        <Dropdown id="dropdown" options={arrLanguages} value={languages} onChange={(e) => setlanguages(e.value)} optionLabel="name"></Dropdown>
                        <label htmlFor="dropdown">Language</label>
                    </span>
                </div>

                <div className="field col-12 md:col-4">
                    <span className="p-float-label">
                        <Dropdown id="dropdown" options={arrClasification} value={clasification} onChange={(e) => setclasification(e.value)} optionLabel="name"></Dropdown>
                        <label htmlFor="dropdown">Clasification</label>
                    </span>
                </div>

                <div className="field col-12 md:col-2">
                    <span className="p-float-label">
                        <InputText type="text" id="hours" value={hours} onChange={onChangeHours} />
                        <label htmlFor="hours">Hours</label>
                    </span>
                </div>

                <div className="field col-12 md:col-2">
                    <span className="p-float-label">
                        <InputText type="text" id="minutes" value={minutes} onChange={onChangeMinutes} />
                        <label htmlFor="minutes">Minutes</label>
                    </span>
                </div>

                <div className="field col-12 md:col-4">
                    <span className="p-float-label">
                        <Calendar inputId="release_date" value={releaseDate} onChange={(e) => setreleaseDate(e.value)}></Calendar>
                        <label htmlFor="release_date">Release date</label>
                    </span>
                </div>

                <div className="field col-12 md:col-4">
                    <span className="p-float-label">
                        <InputText type="text" id="urlTrailer" value={urlTrailer} onChange={(e) => seturlTrailer(e.target.value)} />
                        <label htmlFor="urlTrailer">URL trailer</label>
                    </span>
                </div>

                <div className="field col-12 md:col-6">
                    <span className="p-float-label">
                        <InputTextarea id="synopsis" rows="3" cols="30" value={synopsis} onChange={(e) => setsynopsis(e.target.value)}></InputTextarea>
                        <label htmlFor="synopsis">Synopsis</label>
                    </span>
                </div>

                <div className="field col-12 md:col-6">
                    <span className="p-float-label">
                        <InputTextarea id="distributionFilm" rows="3" cols="30" value={distributionFilm} onChange={(e) => setdistributionFilm(e.target.value)}></InputTextarea>
                        <label htmlFor="distributionFilm">Distribution film</label>
                    </span>
                </div>

                <div className="field col-12 md:col-12">
                    <span className="p-float-label">
                        <InputText type="text" id="directorName" value={directorName} onChange={(e) => setdirectorName(e.target.value)} />
                        <label htmlFor="directorName">Director name</label>
                    </span>
                </div>

                <div className="field col-3 md:col-3">
                    <Button label="Create film" className="p-button-raised mr-2 mb-2" onClick={createFilm} />
                </div>
            </div>
        </div>
    )
}


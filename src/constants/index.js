export const HOST = 'http://localhost:19500/';
export const VERSION = 'api/v1/';

const APP_URL = {
    // ROUTES FILMS
    CREATE_FILM: `${HOST}${VERSION}admin/createFilms`,
    LIST_FILMS: `${HOST}${VERSION}admin/listFilms`,

    // LIST PARAMETERS
    LIST_LANGUAGES: `${HOST}${VERSION}admin/listLanguages`,
    LIST_CLASIFICATION: `${HOST}${VERSION}admin/listClasification`,

}

export default APP_URL
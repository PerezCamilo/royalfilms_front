import APP_URL from "../constants";

class Api {
    async createFilms(nameFilm, language, clasification, duration, release_date, trailer_url, synopsis, director_film, distribution_film) {
        let data = {
            nameFilm,
            language,
            clasification,
            duration,
            release_date,
            trailer_url,
            synopsis,
            director_film,
            distribution_film
        };
        const query = await fetch(`${APP_URL.CREATE_FILM}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return query
    }

    async listLanguajes() {
        const query = await fetch(`${APP_URL.LIST_LANGUAGES}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return query
    }

    async listClasification() {
        const query = await fetch(`${APP_URL.LIST_CLASIFICATION}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return query
    }

    async listFilms() {
        const query = await fetch(`${APP_URL.LIST_FILMS}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        });
        return query
    }
}

export default new Api();